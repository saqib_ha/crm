<?php
$GLOBALS['app_list_strings']['open_to_ep_list']=array (
  'Yes' => '',
  'No' => '',
);

$GLOBALS['app_list_strings']['open_to_ep']=array (
  'Yes' => 'Yes',
  'No' => 'No',
);
$GLOBALS['app_list_strings']['permanent_contract_list']=array (
  'P' => 'Permanent',
  'C' => 'Contract',
);
$GLOBALS['app_list_strings']['payroll_list']=array (
  'IM' => 'Intellect Minds',
  'Client' => 'Client',
);
$GLOBALS['app_list_strings']['notice_period_list']=array (
  '1M' => '01 Month',
  '2M' => '02 Months',
  '3M' => '03 Months',
  '1_2W' => '1-2 Weeks',
  '0M' => 'Immediate',
);
$GLOBALS['app_list_strings']['profile_stage_list']=array (
  'For Future Use' => 'For Future Use',
  'Submitted' => 'Submitted By Recruiter',
  'Rejected by Account Manager' => 'Rejected by account manager',
  'Proposed to client' => 'Proposed to client',
  'Selected for interview' => 'Selected for interview',
  'Rejected by client' => 'Rejected by client',
  'Offered by client' => 'Offered by client',
  'Offer accepted' => 'Offer accepted',
  'Rejected by candidate' => 'Rejected by candidate',
);

$GLOBALS['app_list_strings']['case_status_dom']=array (
  'Urgent' => '01 Week To Close',
  'Open' => '02 Week To Close',
  'New' => '03 Week To Close',
  'Closed' => 'Closed',
  'Hold' => 'Hold',
  'WaitingSales' => 'Enough profiles proposed, Pending on Sales',
  'Onboarding' => 'Client Confirmed, Awaiting On Boarding ',
  'Awaiting' => 'Waiting For Client',
);$app_list_strings['moduleList']['Cases']='Openings';$app_strings['LNK_SEND_EMAIL']='Send Email';
$app_list_strings['moduleListSingular']['Cases']='Opening';
$app_list_strings['record_type_display']['Cases']='Opening';
$app_list_strings['parent_type_display']['Cases']='Opening';
$app_list_strings['record_type_display_notes']['Cases']='Opening';
$GLOBALS['app_list_strings']['send_email_to_manager_list']=array (
  '' => '',
  'No' => 'No',
  'Yes' => 'Yes',
);
$app_list_strings['moduleList']['Notes']='CVs';
$app_list_strings['moduleListSingular']['Notes']='CV';
$GLOBALS['app_list_strings']['visa_status_list']=array (
  '' => '',
  'SG Citizen' => 'SG Citizen',
  'MY Citizen' => 'MY Citizen',
  'SG PR' => 'SG PR',  'MY PR' => 'MY PR',  'SG LTVP' => 'SG LTVP',  'MY LTVP' => 'MY LTVP',  'SG DP' => 'SG DP',  'MY DP' => 'MY DP',  'Others' => 'Others',
);
$GLOBALS['app_list_strings']['parent_type_display']=array (
  'Cases' => 'Opening',
  'Accounts' => 'Account',
  'Contacts' => 'Contact',
  'Tasks' => 'Task',
  'Opportunities' => 'Opportunity',
  'Bugs' => 'Bug',
  'Leads' => 'Lead',
  'Project' => 'Project',
  'ProjectTask' => 'Project Task',
  'Prospects' => 'Target',
);
$GLOBALS['app_list_strings']['record_type_display_notes']=array (
  'Cases' => 'Opening',
  'Accounts' => 'Account',
  'Contacts' => 'Contact',
  'Opportunities' => 'Opportunity',
  'Tasks' => 'Task',
  'Emails' => 'Email',
  'Bugs' => 'Bug',
  'Project' => 'Project',
  'ProjectTask' => 'Project Task',
  'Prospects' => 'Target',
  'Leads' => 'Lead',
  'Meetings' => 'Meeting',
  'Calls' => 'Call',
);