<?php
// created: 2012-05-09 11:17:10
$mod_strings = array (
  'LBL_DESCRIPTION' => 'Additional Info',
  'LBL_CONTACT_INFORMATION' => 'Contact Details',
  'LBL_CASES_SUBPANEL_TITLE' => 'Openings',
  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Contacts',
  'LBL_ASSIGNED_TO_NAME' => 'Account Manager',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'CVs',
);
?>
