<?php
 // created: 2019-12-19 05:29:23
$dictionary['Note']['fields']['reason_to_propose']['name']='reason_to_propose';
$dictionary['Note']['fields']['reason_to_propose']['vname']='LBL_REASON_TO_PROPOSE';
$dictionary['Note']['fields']['reason_to_propose']['type']='text';
$dictionary['Note']['fields']['reason_to_propose']['comment']='Full text of the note';
$dictionary['Note']['fields']['reason_to_propose']['rows']=6;
$dictionary['Note']['fields']['reason_to_propose']['cols']=80;
$dictionary['Note']['fields']['reason_to_propose']['required']=true;
$dictionary['Note']['fields']['reason_to_propose']['inline_edit']=true;
$dictionary['Note']['fields']['reason_to_propose']['help']='Please write down the reason to propose this profile';
$dictionary['Note']['fields']['reason_to_propose']['comments']='Please write down the reason to propose this profile';
$dictionary['Note']['fields']['reason_to_propose']['merge_filter']='disabled';

 ?>