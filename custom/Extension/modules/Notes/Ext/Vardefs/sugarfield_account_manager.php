<?php
 // created: 2019-12-19 05:29:23
$dictionary['Note']['fields']['account_manager'] = array(
	'name' => 'account_manager',
	'type' => 'varchar',
	'dbType' => 'varchar',
	'vname' => 'LBL_ACCOUNT_MANAGER',
	'len' => 255,
	'comment' => 'Account Manager',
	'unified_search' => true,
	'full_text_search' => 
	array (
		'boost' => 3,
	),
	'audited' => false,
	'required' => false,
);

 ?>