<?php
 // created: 2019-12-19 05:31:58
$dictionary['Note']['fields']['filename']['required']=false;
$dictionary['Note']['fields']['filename']['inline_edit']=true;
$dictionary['Note']['fields']['filename']['comments']='File name associated with the note (attachment)';
$dictionary['Note']['fields']['filename']['importable']='true';
$dictionary['Note']['fields']['filename']['merge_filter']='disabled';

 ?>