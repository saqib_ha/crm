<?php
// created: 2019-12-16 02:55:49
$mod_strings = array (
  'LBL_DESCRIPTION' => 'Additional Info',
  'LBL_CONTACT_INFORMATION' => 'Contact Details',
  'LBL_CASES_SUBPANEL_TITLE' => 'Openings',
  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Contacts',
  'LBL_ASSIGNED_TO_NAME' => 'Account Manager',
  'LBL_CASES' => 'Cases',
  'LBL_NOTES' => 'CVs',
  'LBL_TASKS' => 'Tasks',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'CVs',
);