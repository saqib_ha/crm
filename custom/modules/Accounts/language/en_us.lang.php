<?php
// created: 2019-12-16 02:55:49
$mod_strings = array (
  'LBL_PANEL_ASSIGNMENT' => 'Address',
  'LBL_SIGNED_AGREEMENT' => 'Signed Agreement',
  'LBL_ASSIGNED_TO' => 'Account Manager',
  'LBL_DESCRIPTION' => 'Additional Details',
  'LBL_CASES_SUBPANEL_TITLE' => 'Openings',
  'LBL_ACCOUNTS_SUBPANEL_TITLE' => 'None',
  'LBL_LIST_ASSIGNED_USER' => 'Account Manager',
  'LBL_CONTACTS_SUBPANEL_TITLE' => 'Contacts',
  'LBL_CASES' => 'Cases',
  'LBL_NOTES' => 'CVs',
  'LBL_HISTORY_SUBPANEL_TITLE' => 'CVs',
);