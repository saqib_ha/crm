<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2019-12-16 03:21:00
$dictionary['Note']['fields']['current_location_c']['inline_edit']='1';
$dictionary['Note']['fields']['current_location_c']['labelValue']='Current Location';

 

 // created: 2019-12-19 05:31:58
$dictionary['Note']['fields']['filename']['required']=true;
$dictionary['Note']['fields']['filename']['inline_edit']=true;
$dictionary['Note']['fields']['filename']['comments']='File name associated with the note (attachment)';
$dictionary['Note']['fields']['filename']['importable']='true';
$dictionary['Note']['fields']['filename']['merge_filter']='disabled';

 

 // created: 2019-12-19 05:29:23
$dictionary['Note']['fields']['reason_to_propose']['name']='reason_to_propose';
$dictionary['Note']['fields']['reason_to_propose']['vname']='LBL_REASON_TO_PROPOSE';
$dictionary['Note']['fields']['reason_to_propose']['type']='text';
$dictionary['Note']['fields']['reason_to_propose']['comment']='Full text of the note';
$dictionary['Note']['fields']['reason_to_propose']['rows']=6;
$dictionary['Note']['fields']['reason_to_propose']['cols']=80;
$dictionary['Note']['fields']['reason_to_propose']['required']=true;
$dictionary['Note']['fields']['reason_to_propose']['inline_edit']=true;
$dictionary['Note']['fields']['reason_to_propose']['help']='Please write down the reason to propose this profile';
$dictionary['Note']['fields']['reason_to_propose']['comments']='Please write down the reason to propose this profile';
$dictionary['Note']['fields']['reason_to_propose']['merge_filter']='disabled';

 

 // created: 2019-05-17 04:04:09
$dictionary['Note']['fields']['send_email_to_manager_c']['inline_edit']='1';
$dictionary['Note']['fields']['send_email_to_manager_c']['labelValue']='send email to manager';

 

 // created: 2019-12-16 03:22:28
$dictionary['Note']['fields']['visa_status_c']['inline_edit']='1';
$dictionary['Note']['fields']['visa_status_c']['labelValue']='Visa Status';

 
?>