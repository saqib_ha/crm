<?php

global $db;

echo $query = 'select * from notes where deleted = 0';

$result = $db->query($query);
while ($note = $db->fetchByAssoc($result)) {
    $note_id = $note['id'];
	
	$note_bean = BeanFactory::getBean('Notes', $note_id);
	$case_bean = BeanFactory::getBean('Cases', $note_bean->parent_id);
	
	$note_bean->account_manager = $case_bean->assigned_user_name;
	$note_bean->save();
	$note_bean = null;
}
	
	
	die();
?>