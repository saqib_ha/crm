<?php

	$note_id = $_REQUEST["record"];

	$note_obj = new Note();
	$note_obj->retrieve($note_id);
	
	$case_id = $note_obj->parent_id;
	$case_obj = new aCase();
	$case_obj->retrieve($case_id);
	
	$note_obj->assigned_user_id = $case_obj->assigned_user_id;
	$note_obj->send_email_to_manager_c = 'Yes';
	$note_obj->is_manager_email = 1;
	$note_obj->save();
	
	SugarApplication::appendErrorMessage('Email Sent Successfully');
	SugarApplication::redirect('index.php?module=Cases&action=DetailView&record='.$case_id);
		
?>