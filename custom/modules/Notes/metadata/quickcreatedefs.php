<?php
$viewdefs ['Notes'] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'enctype' => 'multipart/form-data',
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'javascript' => '{sugar_getscript file="include/javascript/dashlets.js"}
<script>toggle_portal_flag(); function toggle_portal_flag()  {literal} { {/literal} {$TOGGLE_JS} {literal} } {/literal} </script>',
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_NOTE_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_ASSIGNMENT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL4' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'lbl_note_information' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_SUBJECT',
            'displayParams' => 
            array (
              'size' => 50,
              'required' => true,
            ),
          ),
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'phone_1_c',
            'label' => 'LBL_PHONE_1',
          ),
          1 => 
          array (
            'name' => 'phone_2_c',
            'label' => 'LBL_PHONE_2',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'email_id_1_c',
            'label' => 'LBL_EMAIL_ID_1',
          ),
          1 => 
          array (
            'name' => 'email_id_2_c',
            'label' => 'LBL_EMAIL_ID_2',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'visa_status_c',
            'studio' => 'visible',
            'label' => 'LBL_VISA_STATUS',
          ),
          1 => 
          array (
            'name' => 'current_location_c',
            'studio' => 'visible',
            'label' => 'LBL_CURRENT_LOCATION',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'skill_c',
            'label' => 'LBL_SKILL',
          ),
          1 => 
          array (
            'name' => 'assigned_user_name',
            'label' => 'LBL_ASSIGNED_TO',
          ),
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'reason_to_propose',
            'comment' => 'Full text of the note',
            'label' => 'LBL_REASON_TO_PROPOSE',
			'displayParams' => 
            array (
              'increaseWidth' => 'true',
            ),
          ),
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'address_postalcode_c',
            'label' => 'LBL_ADDRESS_POSTALCODE',
          ),
          1 => 
          array (
            'name' => 'address_country_c',
            'label' => 'LBL_ADDRESS_COUNTRY',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'address_c',
            'label' => 'LBL_ADDRESS',
          ),
          1 => 
          array (
            'name' => 'address_city_c',
            'label' => 'LBL_ADDRESS_CITY',
          ),
        ),
      ),
      'LBL_PANEL_ASSIGNMENT' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'last_salary_c',
            'label' => 'LBL_LAST_SALARY',
          ),
          1 => 
          array (
            'name' => 'expected_salary_c',
            'label' => 'LBL_EXPECTED_SALARY',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'notice_period_c',
            'studio' => 'visible',
            'label' => 'LBL_NOTICE_PERIOD',
          ),
          1 => 'parent_name',
        ),
        2 => 
        array (
          0 => 'filename',
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'label' => 'LBL_NOTE_STATUS',
            'displayParams' => 
            array (
              'rows' => 6,
              'cols' => 75,
            ),
          ),
        ),
      ),
      'lbl_editview_panel4' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'profile_stage_c',
            'studio' => 'visible',
            'label' => 'LBL_PROFILE_STAGE',
          ),
          1 => 
          array (
            'name' => 'interview_schedule_c',
            'label' => 'LBL_INTERVIEW_SCHEDULE',
          ),
        ),
      ),
    ),
  ),
);
;
?>
