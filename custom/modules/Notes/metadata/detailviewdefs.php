<?php
$viewdefs ['Notes'] = 
array (
  'DetailView' => 
  array (
    'templateMeta' => 
    array (
      'form' => 
      array (
        'buttons' => 
        array (
          0 => 'EDIT',
          1 => 'DUPLICATE',
          2 => 'DELETE',
          3 => 
          array (
            'customCode' => '<input id="send_advisory_email" title="Send Email" class="button" type="submit" name="send_advisory_email" value="Send Email" onClick="this.form.action.value=\'sendAdvisoryEmail\';">',
            'sugar_html' => 
            array (
              'type' => 'submit',
              'value' => '{$MOD.LBL_SEND_ADVISORY_EMAIL}',
              'htmlOptions' => 
              array (
                'class' => 'button',
                'id' => 'send_advisory_email',
                'title' => '{$MOD.LBL_SEND_ADVISORY_EMAIL}',
                'onclick' => 'this.form.action.value=\'sendAdvisoryEmail\';',
                'name' => 'send_advisory_email',
              ),
            ),
          ),
        ),
      ),
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => false,
      'tabDefs' => 
      array (
        'LBL_NOTE_INFORMATION' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_PANEL_ASSIGNMENT' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL4' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
      'syncDetailEditViews' => true,
    ),
    'panels' => 
    array (
      'lbl_note_information' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'name',
            'label' => 'LBL_SUBJECT',
          ),
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'phone_1_c',
            'label' => 'LBL_PHONE_1',
          ),
          1 => 
          array (
            'name' => 'phone_2_c',
            'label' => 'LBL_PHONE_2',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'email_id_1_c',
            'label' => 'LBL_EMAIL_ID_1',
          ),
          1 => 
          array (
            'name' => 'email_id_2_c',
            'label' => 'LBL_EMAIL_ID_2',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'visa_status_c',
            'studio' => 'visible',
            'label' => 'LBL_VISA_STATUS',
          ),
          1 => 
          array (
            'name' => 'current_location_c',
            'studio' => 'visible',
            'label' => 'LBL_CURRENT_LOCATION',
          ),
        ),
        4 => 
        array (
          0 => 
          array (
            'name' => 'skill_c',
            'label' => 'LBL_SKILL',
          ),
          1 => 'assigned_user_name',
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'reason_to_propose',
            'comment' => 'Full text of the note',
            'label' => 'LBL_REASON_TO_PROPOSE',
          ),
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'address_postalcode_c',
            'label' => 'LBL_ADDRESS_POSTALCODE',
          ),
          1 => 
          array (
            'name' => 'address_country_c',
            'label' => 'LBL_ADDRESS_COUNTRY',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'address_c',
            'label' => 'LBL_ADDRESS',
          ),
          1 => 
          array (
            'name' => 'address_city_c',
            'label' => 'LBL_ADDRESS_CITY',
          ),
        ),
      ),
      'LBL_PANEL_ASSIGNMENT' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'last_salary_c',
            'label' => 'LBL_LAST_SALARY',
          ),
          1 => 
          array (
            'name' => 'expected_salary_c',
            'label' => 'LBL_EXPECTED_SALARY',
          ),
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'notice_period_c',
            'studio' => 'visible',
            'label' => 'LBL_NOTICE_PERIOD',
          ),
          1 => 
          array (
            'name' => 'parent_name',
            'customLabel' => '{sugar_translate label=\'LBL_MODULE_NAME\' module=$fields.parent_type.value}',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'filename',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'description',
            'label' => 'LBL_NOTE_STATUS',
          ),
        ),
      ),
      'lbl_editview_panel4' => 
      array (
        0 => 
        array (
          0 => 
          array (
            'name' => 'profile_stage_c',
            'studio' => 'visible',
            'label' => 'LBL_PROFILE_STAGE',
          ),
          1 => 
          array (
            'name' => 'interview_schedule_c',
            'label' => 'LBL_INTERVIEW_SCHEDULE',
          ),
        ),
      ),
    ),
  ),
);
;
?>
