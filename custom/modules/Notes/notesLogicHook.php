<?php
if (!defined('sugarEntry') || !sugarEntry)
    die('Not A Valid Entry Point');

class notesLogicHook {

    function beforeSave($bean, $event, $arguments) {
		$bean->send_email_to_manager_c = 'No';
		if(isset($bean->is_manager_email) && $bean->is_manager_email == 1){
			$bean->send_email_to_manager_c = 'Yes';
			$bean->is_manager_email = 0;
		}
		
		// save account manager
		$case_bean = BeanFactory::getBean('Cases', $bean->parent_id);
		$bean->account_manager = $case_bean->assigned_user_name;
	}
}
