<?php 
 //WARNING: The contents of this file are auto-generated


 // created: 2019-05-16 08:50:21
$dictionary['Case']['fields']['description']['inline_edit']=true;
$dictionary['Case']['fields']['description']['comments']='Full text of the description';
$dictionary['Case']['fields']['description']['merge_filter']='disabled';
$dictionary['Case']['fields']['description']['editor']='false';

 

 // created: 2019-05-16 08:50:59
$dictionary['Case']['fields']['jd_advertisement_c']['inline_edit']='1';
$dictionary['Case']['fields']['jd_advertisement_c']['labelValue']='Job Description (Advertisement)';
$dictionary['Case']['fields']['jd_advertisement_c']['rows']='6';
$dictionary['Case']['fields']['jd_advertisement_c']['cols']='80';

 
?>