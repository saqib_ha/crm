<?php
$viewdefs ['Cases'] = 
array (
  'QuickCreate' => 
  array (
    'templateMeta' => 
    array (
      'maxColumns' => '2',
      'widths' => 
      array (
        0 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
        1 => 
        array (
          'label' => '10',
          'field' => '30',
        ),
      ),
      'useTabs' => true,
      'tabDefs' => 
      array (
        'LBL_CASE_INFORMATION' => 
        array (
          'newTab' => true,
          'panelDefault' => 'expanded',
        ),
        'LBL_EDITVIEW_PANEL1' => 
        array (
          'newTab' => false,
          'panelDefault' => 'expanded',
        ),
      ),
    ),
    'panels' => 
    array (
      'lbl_case_information' => 
      array (
        0 => 
        array (
          0 => 'name',
          1 => '',
        ),
        1 => 
        array (
          0 => 'account_name',
          1 => 
          array (
            'name' => 'imcontact_c',
            'studio' => 'visible',
            'label' => 'LBL_IMCONTACT',
          ),
        ),
        2 => 
        array (
          0 => 
          array (
            'name' => 'budget_c',
            'label' => 'LBL_BUDGET',
          ),
          1 => 
          array (
            'name' => 'open_to_ep_c',
            'studio' => 'visible',
            'label' => 'LBL_OPEN_TO_EP',
          ),
        ),
        3 => 
        array (
          0 => 
          array (
            'name' => 'permanent_contract_c',
            'studio' => 'visible',
            'label' => 'LBL_PERMANENT_CONTRACT',
          ),
          1 => 
          array (
            'name' => 'payroll_c',
            'studio' => 'visible',
            'label' => 'LBL_PAYROLL',
          ),
        ),
        4 => 
        array (
          0 => 'description',
        ),
        5 => 
        array (
          0 => 
          array (
            'name' => 'jd_advertisement_c',
            'studio' => 'visible',
            'label' => 'LBL_JD_ADVERTISEMENT',
			'displayParams' => 
            array (
              'increaseWidth' => 'true',
            ),
          ),
        ),
      ),
      'lbl_editview_panel1' => 
      array (
        0 => 
        array (
          0 => 'assigned_user_name',
          1 => '',
        ),
        1 => 
        array (
          0 => 
          array (
            'name' => 'imrecincharge_c',
            'label' => 'LBL_IMRECINCHARGE',
          ),
          1 => 'status',
        ),
      ),
    ),
  ),
);
;
?>
